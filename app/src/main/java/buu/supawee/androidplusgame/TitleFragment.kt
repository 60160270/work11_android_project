package buu.supawee.androidplusgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.supawee.androidplusgame.databinding.FragmentTitleBinding
import buu.supawee.androidplusgame.screens.game.GameViewModel

class TitleFragment : Fragment() {

    private lateinit var viewModel:GameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater,R.layout.fragment_title,container,false)
        binding.btnPlusGame.setOnClickListener { view : View ->
            val action = TitleFragmentDirections.actionTitleFragmentToGameFragment(0)
            view.findNavController().navigate(action)
        }
        binding.btnMinusGame.setOnClickListener { view : View ->
            view.findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToMinusFragment())
        }
        binding.btnMultiplyGame.setOnClickListener { view : View ->
            view.findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToMultiplyFragment())
        }
        setHasOptionsMenu(true)
        return binding.root
    }
}