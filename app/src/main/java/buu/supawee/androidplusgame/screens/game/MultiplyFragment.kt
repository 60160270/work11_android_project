package buu.supawee.androidplusgame.screens.game

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import buu.supawee.androidplusgame.R
import buu.supawee.androidplusgame.databinding.FragmentGameBinding
import buu.supawee.androidplusgame.databinding.FragmentMultiplyBinding
import buu.supawee.androidplusgame.screens.score.ScoreFragmentArgs
import buu.supawee.androidplusgame.screens.score.ScoreViewModelFactory
import kotlinx.android.synthetic.main.fragment_game.*

class MultiplyFragment() : Fragment() {
    private lateinit var binding: FragmentMultiplyBinding
    private lateinit var viewModel:GameViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val preference = PreferenceClass(requireContext())

        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_multiply, container, false)
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)
        viewModel.setTypeOfGame("multi")
        viewModel.setScore(preference.getCorrectPlus())
        viewModel.setScoreMiss(preference.getIncorrectPlus())

        binding.lifecycleOwner = viewLifecycleOwner
        binding.gameViewModel = viewModel

        viewModel._score.observe(viewLifecycleOwner, Observer<Int> { newScore ->
            preference.setCorrect(newScore)
        })
        viewModel._scoreMiss.observe(viewLifecycleOwner, Observer<Int> { newScore ->
            preference.setIncorrect(newScore)
        })
        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer<Boolean> { hasFinished ->
            if(hasFinished) gameFinished()
        })
//        val pref:PreferenceClass = PreferenceClass(requireContext())

        return binding.root
    }
    private fun gameFinished(){
        val action = MultiplyFragmentDirections.actionMultiplyFragmentToScoreFragment()
//        action.score = viewModel.score.value?:0
        NavHostFragment.findNavController(this).navigate(action)
        viewModel.onGameFinishComplete()
    }
}