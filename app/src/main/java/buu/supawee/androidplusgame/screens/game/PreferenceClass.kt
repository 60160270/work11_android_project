package buu.supawee.androidplusgame.screens.game

import android.content.Context
import android.content.SharedPreferences

class PreferenceClass(context: Context){
    val preferenceName = "plus"
    private val PREFERENCE_correctPlusCount = "win"
    private val PREFERENCE_incorrectPlusCount = "lose"

    val preference: SharedPreferences = context.getSharedPreferences(preferenceName,Context.MODE_PRIVATE)

    fun getCorrectPlus() :Int{
        return preference.getInt(PREFERENCE_correctPlusCount.toString(),0)
    }
    fun getIncorrectPlus() :Int{
        return preference.getInt(PREFERENCE_incorrectPlusCount.toString(),0)
    }
    fun setCorrect(correct:Int){
        val editor = preference.edit()
        editor.putInt(PREFERENCE_correctPlusCount.toString(),correct).apply()
    }
    fun setIncorrect(incorrect:Int){
        val editor = preference.edit()
        editor.putInt(PREFERENCE_incorrectPlusCount.toString(),incorrect).apply()
    }
}