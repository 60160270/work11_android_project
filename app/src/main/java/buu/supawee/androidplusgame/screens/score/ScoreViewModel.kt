package buu.supawee.androidplusgame.screens.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScoreViewModel(finalScore:Int,finalScoreMiss:Int): ViewModel() {
    private var _score = MutableLiveData<Int>()
    private var _scoreMiss = MutableLiveData<Int>()
    private val _eventPlayAgain = MutableLiveData<Boolean>()
    val eventPlayAgain: LiveData<Boolean>
        get() {
            return _eventPlayAgain
        }
    val score: LiveData<Int>
        get() {
            return _score
        }
    val scoreMiss: LiveData<Int>
        get() {
            return _scoreMiss
        }

    fun onPlayAgain(){
        _eventPlayAgain.value = true
    }
    fun onPlayAgainComplete(){
        _eventPlayAgain.value = false
    }

    init{
        _score.value = finalScore
        _scoreMiss.value = finalScoreMiss
        Log.i("ScoreViewModal","Final score is $finalScore")
    }
}