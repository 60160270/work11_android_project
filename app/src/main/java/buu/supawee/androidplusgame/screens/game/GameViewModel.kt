package buu.supawee.androidplusgame.screens.game

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GameViewModel():ViewModel(){

    var _typeOfGame = MutableLiveData<String>()
    val typeOfGame: LiveData<String>
        get() {
            return _typeOfGame
        }

    var _txtResult = MutableLiveData<String>()
    val txtResult: LiveData<String>
        get() {
            return _txtResult
        }

    var _resultCorrect = MutableLiveData<Int>()
    val resultCorrect: LiveData<Int>
        get() {
            return _resultCorrect
        }

    var _variable = MutableLiveData<Int>()
    val variable: LiveData<Int>
        get() {
            return _variable
        }
    var _variable2 = MutableLiveData<Int>()
    val variable2: LiveData<Int>
        get() {
            return _variable2
        }

    var _result = MutableLiveData<Int>()
    val result: LiveData<Int>
        get() {
            return _result
        }
    var _result2 = MutableLiveData<Int>()
    val result2: LiveData<Int>
        get() {
            return _result2
        }
    var _result3 = MutableLiveData<Int>()
    val result3: LiveData<Int>
        get() {
            return _result3
        }

    private val _eventGameFinish = MutableLiveData<Boolean>()
    val eventGameFinish: LiveData<Boolean>
        get() {
            return _eventGameFinish
        }

    var _score = MutableLiveData<Int>()
    val score:LiveData<Int>
        get() {
            return _score
        }
    var _scoreMiss = MutableLiveData<Int>()
    val scoreMiss:LiveData<Int>
        get() {
            return _scoreMiss
        }

    fun setTypeOfGame(type:String){
        _typeOfGame.value = type
        Log.i("test","This type of game is ${_typeOfGame.value}")
    }

    fun setScore(number:Int){
        _score.value = number
        Log.i("test","This score of game is ${_score.value}")
    }
    fun setScoreMiss(number:Int){
        _scoreMiss.value = number
        Log.i("test","This score missing of game is ${_scoreMiss.value}")
    }

    fun makePlusResult():Int{
        _variable.value = (0..10).random()
        _variable2.value = (0..10).random()
        return _variable.value!!+_variable2.value!!
    }

    fun makeMinusResult():Int{
        _variable.value = (0..10).random()
        _variable2.value = (0..10).random()
        return _variable.value!!-_variable2.value!!    }
    fun makeMultiResult():Int{
        _variable.value = (0..10).random()
        _variable2.value = (0..10).random()
        return _variable.value!!*_variable2.value!!
    }

    private fun resetAns(){

        when(_typeOfGame.value){
            "plus" ->  _resultCorrect.value = makePlusResult()
            "minus" -> _resultCorrect.value = makeMinusResult()
            "multi" -> _resultCorrect.value = makeMultiResult()
            else -> {
                Log.i("test","type of game error")
            }
        }
//        Log.i("test","${_resultCorrect.value}")
        var choice = (1..3).random()
        when(choice){
            1 -> {
                _result.value = _resultCorrect.value!!
                _result2.value = _resultCorrect.value!!.plus(1)
                _result3.value = _resultCorrect.value!!.plus(2)
            }
            2 -> {
                _result.value = _resultCorrect.value!!.minus(1)
                _result2.value = _resultCorrect.value!!
                _result3.value = _resultCorrect.value!!.plus(1)
            }
            3 -> {
                _result.value = _resultCorrect.value!!.minus(2)
                _result2.value = _resultCorrect.value!!.minus(1)
                _result3.value = _resultCorrect.value!!
            }
        }
    }

    fun checkAns1(){
        if(_result.value == _resultCorrect.value){
            onCorrect()
        }else{
            onIncorrect()
        }
    }
    fun checkAns2(){
        if(_result2.value == _resultCorrect.value){
            onCorrect()
        }else{
            onIncorrect()
        }
    }
    fun checkAns3(){
        if(_result3.value == _resultCorrect.value){
            onCorrect()
        }else{
            onIncorrect()
        }
    }

    fun onCorrect(){
        _score.value = (score.value)?.plus(1)
        _txtResult.value = "Correct"
        nextQuestion()
    }
    fun onIncorrect(){
        _scoreMiss.value = (scoreMiss.value)?.plus(1)
        _txtResult.value = "Incorrect"
        nextQuestion()
    }

    private fun nextQuestion(){
        resetAns()
    }

    fun onGameFinish(){
        _eventGameFinish.value = true
    }

    fun onGameFinishComplete(){
        _eventGameFinish.value = false
    }

    init {
        _variable.value = (0..10).random()
        _variable2.value = (0..10).random()
        _txtResult.value = "Your result"
        _typeOfGame.value = "plus"
        _resultCorrect.value = 0
        _score.value = 0
        _scoreMiss.value = 0
        _result.value = 0
        _result2.value = 0
        _result3.value = 0
        nextQuestion()
        resetAns()
    }

}