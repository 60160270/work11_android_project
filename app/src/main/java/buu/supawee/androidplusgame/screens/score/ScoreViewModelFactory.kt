package buu.supawee.androidplusgame.screens.score

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class ScoreViewModelFactory(private val finalScore:Int,private val finalScoreMiss:Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(ScoreViewModel::class.java)){
            return ScoreViewModel(finalScore,finalScoreMiss) as T
        }
        throw IllegalArgumentException("Unknown ViewModel Class")
    }
}